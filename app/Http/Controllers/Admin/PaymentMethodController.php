<?php

namespace App\Http\Controllers\Admin;

use App\PaymentMethod;
use Illuminate\View\View;

class PaymentMethodController
{
    public function index(): View
    {
        $paymentMethods = PaymentMethod::paginate();

        return view('admin.payment_methods', compact('paymentMethods'));
    }
}
