@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <h1>Cart items ({{ $cart->getItems()->count() }})</h1>
                @include('success_message')
                @if($cart->getItems()->count())

                    <table class="table table-bordered bg-white">
                        <thead>
                        <tr>
                            <th>Product</th>
                            <th>Price</th>
                            <th>Quantity</th>
                            <th>Total</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($cart->getItems() as $item)
                            <tr id="cart-item-{{$item->id}}">
                                <td>{{ $item->product->name }}</td>
                                <td>{{ $item->product->price }} TRY</td>
                                <td>{{ $item->quantity }}</td>
                                <td>{{ $item->total() }} TRY</td>
                                <td>
                                    <form method="POST" action="{{ route('cart.remove', $item->id) }}">
                                        @method('DELETE')
                                        @csrf
                                        <button class="btn btn-danger btn-sm" type="submit">Remove</button>
                                    </form>
                                </td>
                            </tr>

                        @endforeach
                        </tbody>
                        <tfoot>
                        <tr>
                            <td>Total</td>
                            <td colspan="4">{{ $cart->getTotal() }} TRY</td>
                        </tr>
                        <tr>
                            <td colspan="5">
                                <form method="POST" class="float-left" id="cart-form">
                                    @csrf
                                    @method('DELETE')
                                    <button type="button" class="btn btn-danger"
                                            onclick="confirmation(() => $('#cart-form').submit())">Empty Cart
                                    </button>
                                </form>
                                <a href="{{ route('order.index') }}" class="float-right btn btn-primary">Place Order</a>
                            </td>
                        </tr>
                        </tfoot>
                    </table>

                @else
                    <div class="alert alert-info">Cart is empty.</div>
                @endif
            </div>
        </div>
    </div>
@endsection
