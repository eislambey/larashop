# Installation

PHP 7.4+ required

### Clone the repository
```
git clone https://eislambey@bitbucket.org/eislambey/larashop.git
```

### Install dependencies
```
composer install
npm install
```
### Create .env file from .env.example
Copy the example file as .env and put your database config and set APP_URL.

### Generate app key
```
php artisan key:generate 
```
### Run database migartions
```
php artisan migrate --seed
```
`--sed` will create two users, one payment method and thirty products.

### Link storage directory
```
php artisan storage:link
```

### Build assets
```
npm run dev
```

### Start development server
```
php artisan serve
```


# Users
**Email:** admin@example.com

**Password:** bircom123

**Role:** Admin

---
**Email:** johndoe@example.com

**Password:** bircom123

**Role:** User

