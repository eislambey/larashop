@extends('layouts.admin')

@section('content')
    <h1>
        Users ({{ $userCount }})
        <a href="{{ route('admin.user.create') }}" class="btn btn-success float-right">New User</a>
    </h1>

    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            {{ $message }}
        </div>
    @endif

    <table class="table table-striped table-bordered">
        <thead>
        <tr>
            <th>ID</th>
            <th>Name</th>
            <th>Email</th>
            <th>Role</th>
            <th>Action</th>
        </tr>
        </thead>
        <tbody>

        @foreach($users as $user)
            <tr>
                <td>{{ $user->id }}</td>
                <td>{{ $user->name }}</td>
                <td>{{ $user->email }}</td>
                <td>{{ $user->role }}</td>
                <td>
                    <a class="btn btn-sm btn-primary" href="{{ route('admin.user.edit', [$user->id]) }}">Edit</a>
                    <button class="btn btn-danger btn-sm"
                            onclick="confirmation(() => $('#delete-user-{{ $user->id }}').submit())">Delete
                    </button>
                    <form id="delete-user-{{ $user->id }}" action="{{ route('admin.user.destroy', [$user->id]) }}"
                          method="POST">
                        @method("DELETE")
                        @csrf
                    </form>
                </td>
            </tr>
        @endforeach
        </tbody>

        @if ($users->hasPages())
            <tfoot>
            <tr>
                <td colspan="7">
                    {{ $users->links() }}
                </td>
            </tr>
            </tfoot>
        @endif
    </table>
@endsection
