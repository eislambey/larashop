<?php

namespace App\Services;

use App\CartItem;
use App\Product;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\Auth;

class Cart
{
    /**
     * @var CartItem[]|Collection|null
     */
    private ?Collection $items = null;

    public function add(Product $product, int $quantity): void
    {
        $this->items = null;
        $item = CartItem::where('product_id', $product->id)
            ->where('user_id', Auth::id())
            ->first();

        if ($item) {
            $item->quantity += $quantity;
            $item->save();

            return;
        }

        CartItem::insert([
            'user_id' => Auth::id(),
            'product_id' => $product->id,
            'quantity' => $quantity,
        ]);
    }

    public function remove(Product $product): void
    {
        $item = CartItem::where('id', $product->id)
            ->where('user_id', Auth::id())
            ->first();

        if ($item === null) {
            return;
        }

        $this->items = null;
        if ($item->quantity === 1) {
            $item->delete();

            return;
        }

        $item->quantity -= 1;
        $item->update();
    }

    /**
     * @return CartItem[]|Collection
     */
    public function getItems(): Collection
    {
        if (!$this->items) {
            $this->items = CartItem::where('user_id', Auth::id())->get();
        }

        return $this->items;
    }

    public function removeAll(): void
    {
        CartItem::where('user_id', Auth::id())->delete();
        $this->items = null;
    }

    public function getTotal(): float
    {
        return $this->getItems()->reduce(function (float $carry, CartItem $item) {
            return $carry + ($item->product->price * $item->quantity);
        }, 0.0);
    }
}
