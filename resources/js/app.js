require('./bootstrap');
import axios from 'axios'
import AWN from 'awesome-notifications'

const notifier = new AWN({position: 'top-right'})

const addToCart = async function (productId) {
    try {
        await axios.post('/cart', {
            product_id: productId,
            quantity: parseInt(document.querySelector('#quantity').value),
        });
        notifier.success('Added to your cart')
    } catch (e) {
        notifier.alert(e.response.data.message)
    }
}

const confirmation = function (cb) {
    if (confirm("Are you sure?")) {
        cb()
    }
}

window.confirmation = confirmation
window.addToCart = addToCart
