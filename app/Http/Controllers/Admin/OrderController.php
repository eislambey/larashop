<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Order;
use Illuminate\Http\RedirectResponse;
use Illuminate\View\View;

class OrderController extends Controller
{
    public function index(): View
    {
        $orders = Order::orderByDesc('id')->paginate();
        $orderCount = Order::count();

        return view('admin.order.index', compact('orders', 'orderCount'));
    }

    public function approve(Order $order): RedirectResponse
    {
        $order->approve();
        $order->save();

        return \Redirect::back()->with('success', 'Order approved.');
    }

    public function show(Order $order): View
    {
        return view('admin.order.show', compact('order'));
    }
}
