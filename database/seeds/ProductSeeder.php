<?php

use App\Product;
use Illuminate\Database\Seeder;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $amount = 30;
        for ($i = 0; $i < $amount; $i++) {
            factory(Product::class)->create();
        }
    }
}
