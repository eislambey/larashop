@extends('layouts.admin')

@section('content')
    <h1>Order Detail #{{ $order->id }}</h1>

    @include('success_message')

    <div class="card">
        <div class="card-body">
            <h5>Info</h5>
            <ul>
                <li><b>User:</b> {{ $order->user->name }}</li>
                <li><b>Address:</b> {{ $order->address }}</li>
                <li><b>Payment Method:</b> {{ $order->paymentMethod->name }}</li>
                <li><b>Status:</b> <span
                        class="badge badge-{{ $order->status ? 'success': 'warning' }}">{{ $order->statusText() }}</span>
                </li>
                <li><b>Created At:</b> {{ $order->created_at->format('Y-m-d H:i:s') }}</li>
            </ul>

            <h5>Products ({{ $order->items->count() }})</h5>
            <table class="table table-bordered bg-white">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>Name</th>
                    <th>Price</th>
                    <th>Quantity</th>
                </tr>
                </thead>
                <tbody>
                @foreach ($order->items as $item)
                    <tr>
                        <td>{{ $item->id }}</td>
                        <td>{{ $item->name }}</td>
                        <td>{{ $item->price }} TRY</td>
                        <td>{{ $item->quantity }}</td>

                    </tr>
                @endforeach
                </tbody>
                <tfoot>
                <tr>
                    <th>Total</th>
                    <th colspan="3">{{ $order->total }} TRY</th>
                </tr>
                </tfoot>
            </table>
        </div>
@endsection
