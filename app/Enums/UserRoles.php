<?php

namespace App\Enums;

final class UserRoles
{
    public const ADMIN = 'ROLE_ADMIN';
    public const USER = 'ROLE_USER';

    public static function all(): array
    {
        return array_values((new \ReflectionClass(__CLASS__))->getConstants());
    }
}
