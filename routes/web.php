<?php

use App\Http\Controllers\Admin\DashboardController;
use App\Http\Controllers\Admin\PaymentMethodController;
use App\Http\Controllers\CartController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\OrderController;
use App\Http\Controllers\ProductController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Admin;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [HomeController::class, 'index'])->name('home');
Route::get('/product/{product}', [ProductController::class, 'show'])->name('product');

Route::middleware(['auth'])->group(function () {
    // Cart routes
    Route::prefix('cart')->name('cart.')->group(function () {
        Route::get('/', [CartController::class, 'show'])->name('show');
        Route::post('/', [CartController::class, 'add'])->name('add');
        Route::delete('/', [CartController::class, 'removeAll'])->name('remove-all');
        Route::delete('/{product}', [CartController::class, 'remove'])->name('remove');
    });

    // Order routes
    Route::prefix('order')->name('order.')->group(function () {
        Route::get('/', [OrderController::class, 'index'])->name('index');
        Route::post('/', [OrderController::class, 'place'])->name('place');
        Route::get('/list', [OrderController::class, 'list'])->name('list');
        Route::get('/show/{order}', [OrderController::class, 'show'])->name('show');
    });

});

// Admin routes
Route::middleware(['auth', 'verify_admin'])->prefix('admin')->name('admin.')->group(function () {
    Route::get('/', [DashboardController::class, 'index'])->name('dashboard');
    Route::get('/payment-methods', [PaymentMethodController::class, 'index'])->name('payment_methods');

    Route::resource('user', 'Admin\UserController');
    Route::resource('product', 'Admin\ProductController');

    Route::get('/order', [Admin\OrderController::class, 'index'])->name('order.index');
    Route::post('/order/approve/{order}', [Admin\OrderController::class, 'approve'])->name('order.approve');
    Route::get('/order/show/{order}', [Admin\OrderController::class, 'show'])->name('order.show');
});

Auth::routes();
