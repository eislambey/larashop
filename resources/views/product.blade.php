@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <img src="{{ asset($product->image)  }}" alt="{{ $product->name }}" style="width: 100%; height: 100%">
            </div>
            <div class="col-md-8">
                <div class="card">
                    <div class="card-body">
                        <h2 class="card-title">{{ $product->name }}</h2>
                        <p>{{ $product->description }}</p>
                    </div>

                    <div class="card-footer">
                        <div class="product-price">{{ $product->price }} TRY</div>
                        <label for="quantity">Quantity</label>
                        <div class="row">
                            <div class="col-md-6">
                                <input type="number" id="quantity" value="1" min="1" max="10" class="form-control">
                            </div>
                            <div class="col-md-6">
                                <button class="btn btn-primary" onclick="addToCart({{$product->id}})" @guest disabled @endguest>Add to Cart
                                </button>
                                @guest
                                <span class="text-muted">Login to add cart</span>
                                @endguest
                            </div>
                        </div>

                    </div>

                </div>
            </div>

        </div>
    </div>
@endsection
