<?php

/** @var Factory $factory */

use App\Product;
use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factory;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Storage;

$factory->define(Product::class, function (Faker $faker) {
    $price = rand(10, 100) . '.' . rand(0, 99);

    $imageUrl = 'https://picsum.photos/200/200?' . mt_rand();

    $imagePath = "products/" . Str::random() . ".jpg";
    Storage::put($imagePath, file_get_contents($imageUrl));

    return [
        'name' => $faker->name,
        'description' => $faker->text(500),
        'image' => $imagePath,
        'price' => (float)$price,
    ];
});
