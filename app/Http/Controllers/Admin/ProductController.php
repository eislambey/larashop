<?php

namespace App\Http\Controllers\Admin;

use App\CartItem;
use App\Http\Controllers\Controller;
use App\Http\Requests\StoreProductRequest;
use App\Http\Requests\UpdateProductRequest;
use App\Product;
use Exception;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\RedirectResponse;
use Illuminate\View\View;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return View
     */
    public function index(): View
    {
        $productCount = Product::count();
        $products = Product::orderByDesc('id')->paginate();

        return view('admin.product.index', compact('productCount', 'products'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return View
     */
    public function create(): View
    {
        return view('admin.product.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreProductRequest $request
     * @return RedirectResponse
     */
    public function store(StoreProductRequest $request): RedirectResponse
    {
        $image = $request->file('image')->storePublicly('products');

        $product = $request->validated();
        $product['image'] = $image;

        Product::insert($product);

        return \Redirect::route('admin.product.index')->with('success', 'Product created.');
    }

    /**
     * Display the specified resource.
     *
     * @param Product $product
     * @return void
     */
    public function show(Product $product)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Product $product
     * @return View
     */
    public function edit(Product $product): View
    {
        return view('admin.product.edit', compact('product'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateProductRequest $request
     * @param Product $product
     * @return RedirectResponse
     */
    public function update(UpdateProductRequest $request, Product $product): RedirectResponse
    {
        $data = $request->validated();

        if ($request->has('image')) {
            \Storage::delete($product->image);
            $data['image'] = $request->file('image')->storePublicly('products');
        }

        $product->update($data);

        return \Redirect::back()->with('success', 'Product updated.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Product $product
     * @return RedirectResponse
     * @throws Exception
     */
    public function destroy(Product $product): RedirectResponse
    {
        $product->delete();

        // remove from cart_items to
        CartItem::where('product_id', $product->id)->delete();

        return \Redirect::back()->with('success', 'Product deleted');
    }
}
