<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * App\CartItem
 *
 * @property int $id
 * @property int $user_id
 * @property int $product_id
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CartItem newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CartItem newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CartItem query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CartItem whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CartItem whereProductId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CartItem whereUserId($value)
 * @mixin \Eloquent
 * @property-read Product $product
 * @property int $quantity
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CartItem whereQuantity($value)
 */
class CartItem extends Model
{
    public $timestamps = false;

    public function product(): BelongsTo
    {
        return $this->belongsTo(Product::class);
    }

    public function total(): float
    {
        $total = $this->product->price * $this->quantity;

        return round($total, 2);
    }
}
