@extends('layouts.admin')

@section('content')
    <h1>Stats</h1>
    <table class="table table-striped table-bordered">
        <tbody>
        <tr>
            <td>Order count</td>
            <td>{{ $userCount }}</td>
        </tr>
        <tr>
            <td>User count</td>
            <td>{{ $userCount }}</td>
        </tr>
        <tr>
            <td>Product count</td>
            <td>{{ $productCount }}</td>
        </tr>
        </tbody>
    </table>
@endsection
