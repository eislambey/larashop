<?php

namespace App\Http\Controllers;

use App\Http\Requests\PlaceOrderRequest;
use App\Order;
use App\OrderItem;
use App\PaymentMethod;
use App\Services\Cart;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\View\View;

class OrderController extends Controller
{
    public function index(Cart $cart): View
    {
        $paymentMethods = PaymentMethod::all();

        return view('order.index', compact('cart', 'paymentMethods'));
    }

    public function place(PlaceOrderRequest $request, Cart $cart): RedirectResponse
    {
        $data = $request->validated();
        $data['user_id'] = \Auth::id();
        $data['total'] = $cart->getTotal();

        DB::beginTransaction();

        try {
            $order = Order::create($data);

            foreach ($cart->getItems() as $item) {
                OrderItem::create([
                    'order_id' => $order->id,
                    'name' => $item->product->name,
                    'image' => $item->product->image,
                    'price' => $item->product->price,
                    'description' => $item->product->description,
                    'quantity' => $item->quantity,
                ]);
                $cart->remove($item->product);
            }

            $cart->removeAll();

            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();

            Log::error('Order Place', [
                'message' => $e->getMessage(),
            ]);

            return \Redirect::back()->with('error', 'Can not place order. Please try again');
        }

        return \Redirect::route('order.show', [$order->id])->with('success', 'Order placed.');
    }

    public function list(): View
    {
        $orders = Order::where('user_id', \Auth::id())->orderByDesc('id')->paginate();
        $orderCount = Order::where('user_id', \Auth::id())->count();

        return view('order.list', compact('orders', 'orderCount'));
    }

    public function show(Order $order): View
    {
        $order->load('items');

        return view('order.show', compact('order'));
    }
}
