<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\PaymentMethod
 *
 * @method static \Illuminate\Database\Eloquent\Builder|\App\PaymentMethod newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\PaymentMethod newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\PaymentMethod query()
 * @mixin \Eloquent
 * @property int $id
 * @property string $name
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\PaymentMethod whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\PaymentMethod whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\PaymentMethod whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\PaymentMethod whereUpdatedAt($value)
 */
class PaymentMethod extends Model
{
    protected $fillable = ['name'];
}
