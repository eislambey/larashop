@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1>Products</h1>
            </div>
            @foreach($products as $product)
                <div class="col-md-4">
                    <figure class="card card-product">
                        <div class="img-wrap">
                            <a href="{{ route('product', [$product->id]) }}">
                                <img src="{{ asset($product->image)  }}" alt="{{ $product->name }}">
                            </a>
                        </div>
                        <figcaption class="info-wrap">
                            <h4 class="title">{{ $product->name }}</h4>
                        </figcaption>
                        <div class="bottom-wrap">
                            <a href="{{ route('product', [$product->id]) }}" class="btn btn-sm btn-primary float-right">Order
                                Now</a>
                            <div class="price-wrap h5">
                                <span class="price-new">{{ $product->price }} TRY</span>
                            </div> <!-- price-wrap.// -->
                        </div> <!-- bottom-wrap.// -->
                    </figure>
                </div> <!-- col // -->
            @endforeach
        </div>

        <!-- pagination -->
        {{ $products->links() }}
    </div>
@endsection
