@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <h1>
                    Place Order
                    <a class="btn btn-secondary float-right" href="{{ route('cart.show') }}">Back to Cart</a>
                </h1>
                @include('success_message')
                @include('validation_errors')

                @if($cart->getItems()->count())
                    <form class="card" action="{{ route('order.place') }}" method="POST">
                        @csrf
                        <div class="card-body">
                            <h5>Order items</h5>
                            <ul>
                            @foreach($cart->getItems() as $item)
                                <li>{{ $item->product->name }} x{{ $item->quantity }} ({{ $item->total() }} TRY)
                                </li>
                            @endforeach
                            </ul>
                            <p>Total: <b>{{ $cart->getTotal() }} TRY</b></p>
                            <div class="form-group">
                                <label for="address">Address</label>
                                <textarea name="address" id="address" class="form-control"
                                          placeholder="Where to deliver">{{ old('address') }}</textarea>
                            </div>

                            <div class="form-group">
                                <label for="payment-method">Payment Method</label>
                                <select name="payment_method_id" id="payment-method" class="form-control">
                                    @foreach($paymentMethods as $method)
                                        <option value="{{ $method->id }}">{{ $method->name }}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="form-group">
                                <button class="float-right btn btn-primary">Place Order</button>
                            </div>
                        </div>
                    </form>
                @else
                    <div class="alert alert-info">Cart is empty.</div>
                @endif
            </div>
        </div>
    </div>
@endsection
