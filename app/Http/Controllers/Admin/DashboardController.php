<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Product;
use App\User;
use Illuminate\View\View;

class DashboardController extends Controller
{
    public function index(): View
    {
        $userCount = User::count();
        $productCount = Product::count();

        return view('admin/dashboard', compact('userCount', 'productCount'));
    }
}
