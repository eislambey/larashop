@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <h1>Order Detail #{{ $order->id }}</h1>

                @include('success_message')

                <div class="card">
                    <div class="card-body">
                        <h5>Info</h5>
                        <ul>
                            <li><b>Address:</b> {{ $order->address }}</li>
                            <li><b>Payment Method:</b> {{ $order->paymentMethod->name }}</li>
                            <li><b>Status:</b> <span class="badge badge-{{ $order->status ? 'success': 'warning' }}">{{ $order->statusText() }}</span></li>
                            <li><b>Created at:</b> {{ $order->created_at }}</li>
                        </ul>

                        <h5>Products ({{ $order->items->count() }})</h5>
                        <table class="table table-bordered bg-white">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>Name</th>
                                <th>Price</th>
                                <th>Quantity</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach ($order->items as $item)
                                <tr>
                                    <td>{{ $item->id }}</td>
                                    <td>{{ $item->name }}</td>
                                    <td>{{ $item->price }} TRY</td>
                                    <td>{{ $item->quantity }}</td>

                                </tr>
                            @endforeach
                            </tbody>
                            <tfoot>
                            <tr>
                                <th>Total</th>
                                <th colspan="3">{{ $order->total }} TRY</th>
                            </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>

                <br>
                <a href="{{ route('order.list') }}" class="btn btn-secondary">Back to Orders</a>
            </div>
        </div>
    </div>
@endsection
