@extends('layouts.admin')

@section('content')
    <h1>
        Products ({{ $productCount }})
        <a href="{{ route('admin.product.create') }}" class="btn btn-success float-right">New Product</a>
    </h1>


    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            {{ $message }}
        </div>
    @endif

    <table class="table table-striped table-bordered">
        <thead>
        <tr>
            <th>ID</th>
            <th>Name</th>
            <th>Price</th>
            <th>Action</th>
        </tr>
        </thead>
        <tbody>

        @foreach($products as $product)
            <tr>
                <td>{{ $product->id }}</td>
                <td>{{ $product->name }}</td>
                <td>{{ $product->getPrice() }}</td>
                <td>
                    <a class="btn btn-sm btn-primary" href="{{ route('admin.product.edit', [$product->id]) }}">Edit</a>
                    <button class="btn btn-danger btn-sm"
                            onclick="confirmation(() => $('#delete-product-{{ $product->id }}').submit())">Delete
                    </button>
                    <form id="delete-product-{{ $product->id }}"
                          action="{{ route('admin.product.destroy', [$product->id]) }}" method="POST">
                        @method("DELETE")
                        @csrf
                    </form>
                </td>
            </tr>
        @endforeach
        </tbody>
        @if ($products->hasPages())
            <tfoot>
            <tr>
                <td colspan="7">
                    {{ $products->links() }}
                </td>
            </tr>
            </tfoot>
        @endif
    </table>
@endsection
