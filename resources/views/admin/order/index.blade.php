@extends('layouts.admin')

@section('content')
    <h1>Orders ({{ $orderCount }})</h1>

    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            {{ $message }}
        </div>
    @endif

    @if ($orders->count())

    <table class="table table-striped table-bordered">
            <thead>
            <tr>
                <th>ID</th>
                <th>User</th>
                <th>Items</th>
                <th>Total</th>
                <th>Action</th>
            </tr>
            </thead>
            <tbody>

            @foreach($orders as $order)
                <tr>
                    <td>{{ $order->id }}</td>
                    <td>{{ $order->user->name }}</td>
                    <td>{{ $order->items->count() }}</td>
                    <td>{{ $order->total }}</td>
                    <td>
                        <a href="{{ route('admin.order.show', [$order->id]) }}"
                           class="btn btn-secondary btn-sm">Details</a>
                        @if (!$order->status)
                            <form action="{{ route('admin.order.approve', [$order->id]) }}" method="POST" class="d-inline-block">
                                @csrf
                                <button class="btn btn-success btn-sm" type="submit">Approve</button>
                            </form>
                        @else
                            <button class="btn btn-success btn-sm" disabled>Approved</button>
                        @endif
                    </td>
                </tr>
            @endforeach
            </tbody>
            @if ($orders->hasPages())
                <tfoot>
                <tr>
                    <td colspan="7">
                        {{ $orders->links() }}
                    </td>
                </tr>
                </tfoot>
            @endif
        </table>
    @else
        <div class="alert alert-info">No order found.</div>
    @endif
@endsection
