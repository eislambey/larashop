@extends('layouts.admin')

@section('content')
    <h1>Payment Methods</h1>
    <table class="table table-striped table-bordered">
        <thead>
        <tr>
            <th>ID</th>
            <th>Name</th>
        </tr>
        </thead>
        <tbody>
        @foreach($paymentMethods as $method)
            <tr>
                <td>{{ $method->id }}</td>
                <td>{{ $method->name }}</td>
            </tr>
        @endforeach
        </tbody>
        @if ($paymentMethods->hasPages())
            <tfoot>
            <tr>
                <td colspan="2">
                    {{ $paymentMethods->links() }}
                </td>
            </tr>
            </tfoot>
        @endif
    </table>
@endsection
