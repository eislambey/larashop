@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <h1>Orders ({{ $orderCount }})</h1>

                @if ($orders->isEmpty())
                    <div class="alert alert-info">No orders.</div>
                @else
                    <table class="table table-bordered bg-white">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>Items</th>
                            <th>Total</th>
                            <th>Date</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($orders as $order)
                            <tr>
                                <td>{{ $order->id }}</td>
                                <td>{{ $order->items->count() }}</td>
                                <td>{{ $order->total }} TRY</td>
                                <td>{{ $order->created_at }}</td>
                                <td>
                                    <a href="{{ route('order.show', [$order->id]) }}"
                                       class="btn btn-primary btn-sm">Details</a>
                                    @if ($order->status)
                                        <button class="btn btn-success btn-sm" disabled>Approved</button>
                                    @else
                                        <button class="btn btn-warning btn-sm" disabled>Pending</button>
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                        @if ($orders->hasPages())
                            <tfoot>
                            <tr>
                                <td colspan="5">{{ $orders->links() }}</td>
                            </tr>
                            </tfoot>
                        @endif
                    </table>
                @endif
            </div>
        </div>
    </div>
@endsection
