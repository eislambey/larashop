<?php

namespace App\Http\Controllers;

use App\Product;
use Illuminate\View\View;

class HomeController extends Controller
{
    public function index(): View
    {
        $products = Product::orderByDesc('id')->paginate();
        $productCount = Product::count();

        return view('welcome', compact('products', 'productCount'));
    }
}
