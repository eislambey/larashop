<?php

use App\Enums\UserRoles;
use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    public function run(): void
    {
        // Admin
        User::insert([
            'name' => 'Emre Can İslambey',
            'email' => 'admin@example.com',
            'password' => Hash::make('bircom123'),
            'role' => UserRoles::ADMIN,
        ]);

        // Regular user
        User::insert([
            'name' => 'John Doe',
            'email' => 'johndoe@example.com',
            'password' => Hash::make('bircom123'),
            'role' => UserRoles::USER,
        ]);
    }
}
