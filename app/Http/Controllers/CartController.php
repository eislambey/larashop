<?php

namespace App\Http\Controllers;

use App\Http\Requests\AddCartItemRequest;
use App\Product;
use App\Services\Cart;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Redirect;
use Illuminate\View\View;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class CartController extends Controller
{
    public function show(Cart $cart): View
    {
        return view('cart.show', compact('cart'));
    }

    public function add(AddCartItemRequest $request, Cart $cart): Response
    {
        /** @var Product $product */
        $product = Product::where('id', $request->get('product_id'))->first();

        if ($product === null) {
            throw new NotFoundHttpException();
        }

        $cart->add($product, $request->get('quantity'));

        return new Response(Response::HTTP_CREATED);
    }

    public function remove(Product $product, Cart $cart): RedirectResponse
    {
        $cart->remove($product);

        return Redirect::back()->with('success', 'Item removed.');
    }

    public function removeAll(Cart $cart): RedirectResponse
    {
        $cart->removeAll();

        return Redirect::back();
    }
}
